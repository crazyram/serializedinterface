﻿using CrazyRam.Core.Serialization;
using UnityEditor;
using UnityEngine;

namespace CrazyRam.Editor
{
    [CustomPropertyDrawer(typeof(SerializedComponentAttribute))]
    public class SerializeComponentAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(attribute is SerializedComponentAttribute serializedAttribute) ||
                !string.Equals(property.name, serializedAttribute.PropertyName))
            {
                base.OnGUI(position, property, label);
                return;
            }

            var obj = property.objectReferenceValue != null
                ? ((MonoBehaviour) property.objectReferenceValue).gameObject
                : null;
            obj = EditorGUI.ObjectField(position, label, obj, typeof(GameObject), true) as GameObject;
            if (obj != null)
            {
                var component = obj.GetComponent(serializedAttribute.Type);
                property.objectReferenceValue = component;
            }
            else
                property.objectReferenceValue = null;
        }
    }
}
