﻿using UnityEngine;
using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Serialization
{
    [System.Serializable]
    public class SerializedComponent<T> where T : class
    {
        protected virtual Component Component
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get { return null; }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T GetItem()
        {
            return Component as T;
        }
    }
}
