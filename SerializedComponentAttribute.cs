﻿using System;
using UnityEngine;

namespace CrazyRam.Core.Serialization
{
    public class SerializedComponentAttribute : PropertyAttribute
    {
        public readonly Type Type;

        public readonly string PropertyName;

        public SerializedComponentAttribute(Type type, string name = "_component")
        {
            Type = type;
            PropertyName = name;
        }
    }
}
